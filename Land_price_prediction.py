import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
data = pd.read_csv('LR.csv')
print(data.shape)
x = data['sqft']
y = data['price(L)']
x_matrix = x.values.reshape(-1,1)
x_matrix.shape
reg = LinearRegression()
reg.fit(x_matrix, y)
plt.scatter(x,y)
yhat = reg.coef_ * x_matrix + reg.intercept_
fig = plt.plot(x, yhat, lw=4, c='red')
plt.xlabel('Square feet')
plt.ylabel('Price in lakhs')
plt.show()
